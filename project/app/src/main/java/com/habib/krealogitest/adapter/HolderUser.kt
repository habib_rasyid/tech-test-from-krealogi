package com.habib.krealogitest.adapter

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.habib.krealogitest.model.UserModel
import com.habib.krealogitest.model.UserModelElement
import kotlinx.android.synthetic.main.item_user.view.*

/**
 * Created by mhabibr
 * Since 14,June,2021
 */
class HolderUser(view: View) : RecyclerView.ViewHolder(view) {
    private val tvUserName = view.txtUserName

    fun bindHero(hero: UserModelElement) {
        tvUserName.text = hero.login
    }
}