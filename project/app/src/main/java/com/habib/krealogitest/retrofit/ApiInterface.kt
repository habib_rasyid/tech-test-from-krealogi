package com.habib.krealogitest.retrofit

import com.habib.krealogitest.model.UserModel
import retrofit2.Call
import retrofit2.http.GET

/**
 * Created by mhabibr
 * Since 13,June,2021
 */
interface ApiInterface {
    @GET("users")
    fun getServices():Call<UserModel>
}