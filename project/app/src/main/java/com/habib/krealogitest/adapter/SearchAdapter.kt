package com.habib.krealogitest.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.habib.krealogitest.R
import com.habib.krealogitest.model.SearchWord

/**
 * Created by mhabibr
 * Since 14,June,2021
 */
class SearchAdapter : ListAdapter<SearchWord, SearchAdapter.SearchViewHolder>(WordsComparator()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SearchViewHolder {
        return SearchViewHolder.create(parent)
    }

    override fun onBindViewHolder(holder: SearchViewHolder, position: Int) {
        val current = getItem(position)
        holder.bind(current.word)
    }

    class SearchViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val wordItemView: TextView = itemView.findViewById(R.id.textView)

        fun bind(text: String?) {
            wordItemView.text = text
        }

        companion object {
            fun create(parent: ViewGroup): SearchViewHolder {
                val view: View = LayoutInflater.from(parent.context)
                    .inflate(R.layout.search_item, parent, false)
                return SearchViewHolder(view)
            }
        }
    }

    class WordsComparator : DiffUtil.ItemCallback<SearchWord>() {
        override fun areItemsTheSame(oldItem: SearchWord, newItem: SearchWord): Boolean {
            return oldItem === newItem
        }

        override fun areContentsTheSame(oldItem: SearchWord, newItem: SearchWord): Boolean {
            return oldItem.word == newItem.word
        }
    }
}