package com.habib.krealogitest.room

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.habib.krealogitest.dao.SearchDao
import com.habib.krealogitest.model.SearchWord

/**
 * Created by mhabibr
 * Since 14,June,2021
 */
// Annotates class to be a Room Database with a table (entity) of the Word class
@Database(entities = arrayOf(SearchWord::class), version = 1, exportSchema = false)
public abstract class SearchRoomDatabase : RoomDatabase() {

    abstract fun searchDao(): SearchDao

    companion object {
        // Singleton prevents multiple instances of database opening at the
        // same time.
        @Volatile
        private var INSTANCE: SearchRoomDatabase? = null

        fun getDatabase(context: Context): SearchRoomDatabase {
            // if the INSTANCE is not null, then return it,
            // if it is, then create the database
            return INSTANCE ?: synchronized(this) {
                val instance = Room.databaseBuilder(
                    context.applicationContext,
                    SearchRoomDatabase::class.java,
                    "search_database"
                ).build()
                INSTANCE = instance
                // return instance
                instance
            }
        }
    }
}