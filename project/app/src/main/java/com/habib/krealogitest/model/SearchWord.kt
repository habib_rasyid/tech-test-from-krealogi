package com.habib.krealogitest.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

/**
 * Created by mhabibr
 * Since 14,June,2021
 */
@Entity(tableName = "search_table")
class SearchWord(@PrimaryKey @ColumnInfo(name = "searchWord") val word: String)