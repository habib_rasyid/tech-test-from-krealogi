package com.habib.krealogitest.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.habib.krealogitest.model.SearchWord
import kotlinx.coroutines.flow.Flow

/**
 * Created by mhabibr
 * Since 14,June,2021
 */
@Dao
interface SearchDao {

    @Query("SELECT * FROM search_table ORDER BY searchWord ASC")
    fun getAlphabetizedWords(): Flow<List<SearchWord>>

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insert(word: SearchWord)

    @Query("DELETE FROM search_table")
    suspend fun deleteAll()
}