package com.habib.krealogitest.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.habib.krealogitest.model.UserModel
import com.habib.krealogitest.repository.MainActivityRepository

/**
 * Created by mhabibr
 * Since 13,June,2021
 */
open class ViewModel:ViewModel() {
    var servicesLiveData:MutableLiveData<UserModel>? = null

    fun getUser():LiveData<UserModel>?{
        servicesLiveData = MainActivityRepository.getServiceApiCall()
        return servicesLiveData
    }
}