package com.habib.krealogitest.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import androidx.recyclerview.widget.RecyclerView
import com.habib.krealogitest.R
import com.habib.krealogitest.model.UserModel
import com.habib.krealogitest.model.UserModelElement
import java.util.*
import kotlin.collections.ArrayList

/**
 * Created by mhabibr
 * Since 14,June,2021
 */
class   UserNameAdapter(private val heroes: ArrayList<UserModelElement>) : RecyclerView.Adapter<HolderUser>() {
    var userFilterList = ArrayList<UserModelElement>()

    init {
        userFilterList = heroes
    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, p1: Int): HolderUser {
        return HolderUser(
            LayoutInflater.from(viewGroup.context).inflate(R.layout.item_user, viewGroup, false)
        )
    }

    override fun getItemCount(): Int = userFilterList.size

    override fun onBindViewHolder(holder: HolderUser, position: Int) {
        holder.bindHero(userFilterList[position])
    }
    @ExperimentalStdlibApi
    fun getFilter(): Filter {
        return object : Filter() {
            override fun performFiltering(constraint: CharSequence?): FilterResults {
                val charSearch = constraint.toString()
                if (charSearch.isEmpty()) {
                    userFilterList = heroes
                } else {
                    val resultList = ArrayList<UserModelElement>()
                    for (row in heroes) {
                        if (row.login.lowercase(Locale.ROOT).contains(charSearch.lowercase(Locale.ROOT))) {
                            resultList.add(row)
                        }
                    }
                    userFilterList = resultList
                }
                val filterResults = FilterResults()
                filterResults.values = userFilterList
                return filterResults
            }

            @Suppress("UNCHECKED_CAST")
            override fun publishResults(constraint: CharSequence?, results: FilterResults?) {
                userFilterList = results?.values as ArrayList<UserModelElement>
                notifyDataSetChanged()
            }

        }
    }

}
