package com.habib.krealogitest.view

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.widget.SearchView
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.habib.krealogitest.R
import com.habib.krealogitest.adapter.UserNameAdapter
import com.habib.krealogitest.viewmodel.ViewModel
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {
    lateinit var context: Context
    lateinit var viewModel: ViewModel
    var userAdapter: UserNameAdapter? = null
    @ExperimentalStdlibApi
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        context = this@MainActivity
        viewModel = ViewModelProvider(this).get(ViewModel::class.java)
        wp7progressBar.showProgressBar()
        viewModel.getUser()!!.observe(this, Observer { serviceSetterGetter ->

            wp7progressBar.hideProgressBar()

//            val userName = serviceSetterGetter[1].login
            val dataList = serviceSetterGetter
            userAdapter = UserNameAdapter(dataList)
//            lblResponse.text = userName
            rvMain.apply {
                layoutManager = LinearLayoutManager(this@MainActivity)
                adapter = userAdapter
            }
        })
        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String): Boolean {
                return false
            }
            override fun onQueryTextChange(newText: String): Boolean {
                userAdapter?.getFilter()?.filter(newText)
                return false
            }
        })

    }
}