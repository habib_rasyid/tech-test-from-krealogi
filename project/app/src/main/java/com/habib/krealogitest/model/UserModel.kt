package com.habib.krealogitest.model

/**
 * Created by mhabibr
 * Since 13,June,2021
 */
typealias UserModel = ArrayList<UserModelElement>

data class UserModelElement (
    val login: String,
    val id: Long,
    val nodeID: String,
    val avatarURL: String,
    val gravatarID: String,
    val url: String,
    val htmlURL: String,
    val followersURL: String,
    val followingURL: String,
    val gistsURL: String,
    val starredURL: String,
    val subscriptionsURL: String,
    val organizationsURL: String,
    val reposURL: String,
    val eventsURL: String,
    val receivedEventsURL: String,
    val type: Type,
    val siteAdmin: Boolean
)
enum class Type {
    Organization,
    User
}