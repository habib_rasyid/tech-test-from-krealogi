package com.habib.krealogitest.retrofit

import com.habib.krealogitest.BuildConfig
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import okhttp3.logging.HttpLoggingInterceptor.Level
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

/**
 * Created by mhabibr
 * Since 13,June,2021
 */
object RetrofitClient {
    const val BASE_URL = "https://api.github.com/"

    val retrofitClient: Retrofit.Builder by lazy {
        val levelType:Level
        if(BuildConfig.BUILD_TYPE.contentEquals("debug"))
            levelType = Level.BODY else levelType = Level.NONE

        val logging = HttpLoggingInterceptor()
        logging.setLevel(levelType)

        val okHttpClient = OkHttpClient.Builder()
        okHttpClient.addInterceptor(logging)

        Retrofit.Builder().baseUrl(BASE_URL)
        .client(okHttpClient.build())
        .addConverterFactory(
            GsonConverterFactory.create()
        )
    }
    val apiInterface: ApiInterface by lazy {
        retrofitClient
            .build()
            .create(ApiInterface::class.java)
    }
}