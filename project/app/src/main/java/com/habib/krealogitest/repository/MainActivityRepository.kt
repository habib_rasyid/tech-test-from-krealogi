package com.habib.krealogitest.repository

import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.habib.krealogitest.model.UserModel
import com.habib.krealogitest.retrofit.RetrofitClient
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * Created by mhabibr
 * Since 13,June,2021
 */
object MainActivityRepository {
    val userModel = MutableLiveData<UserModel>()
    fun getServiceApiCall():MutableLiveData<UserModel>{
        val call = RetrofitClient.apiInterface.getServices()
        call.enqueue(object : Callback<UserModel> {
            override fun onFailure(call: Call<UserModel>, t: Throwable) {
                // TODO("Not Yet Implemented")
                Log.v("DEBUG: ", t.message.toString())
            }

            override fun onResponse(
                call: Call<UserModel>,
                response: Response<UserModel>
            ) {
                // TODO("Not yet implemented")
                Log.v("DEBUG : ", response.body().toString())

                val data = response.body()

                val login = arrayOf(data!!)

                userModel.value = UserModel(login[0])
            }
        })
        return userModel
    }
}