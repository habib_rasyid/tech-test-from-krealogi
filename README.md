# tech-test-from-krealogi

Tech Test from Krealogi

```
Goal:
- Create an android application to fetch user accounts from github.
- Your apps will have one EditText on top of the page so users can type and search, 
  and your application will display a list of accounts whose names start with the letters typed by user.
```

```
Requirements:

- Use Kotlin
- Endless scrolling
- Save search keywords up to 10 (database).
- Error handling

```


